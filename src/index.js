import React from "react";
import {Provider} from "react-redux";

import createStore from "./config/store";
import NavigationControl from "./navigation";

const App = () => (
    <Provider store={createStore()}>
        <NavigationControl/>
    </Provider>
)

export default App;