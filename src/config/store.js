import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

import Reducers from "./../reducers";

export default ( initialState ) => (
    createStore(
        Reducers,
        initialState,
        applyMiddleware( ReduxThunk )
    )
);