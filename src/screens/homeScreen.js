import React, {Component} from "react";
import {StyleSheet,View,Text} from "react-native";

import StatusBar from "./../components/statusBar";

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      padding: 10
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
})

class Home extends Component {
  render(){
    return (
      <View>
          <StatusBar/>
          <View style={styles.container}>
            <Text style={styles.welcome}>Welcome</Text>
          </View>
      </View>
    )
  }
}

Home.navigationOptions = {
  headerTitle: "Home Screen"
}

export default Home;