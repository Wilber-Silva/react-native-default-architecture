import {StyleSheet} from "react-native";

export default StyleSheet.create({
    container: {
            backgroundColor: "red",
            width: '100%',
    },
    top: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
    },
    bottom: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
    },
})