import React from "react";
import {createAppContainer} from "react-navigation";
import {View} from "react-native";

import {
    stackNavigator as stack,
    tabNavigator as tab,
    drawerNavigator as drawer
} from "./screens";

const ContainerStack = createAppContainer(stack)
const ContainerTab = createAppContainer(tab)
const ContainerDrawer = createAppContainer(drawer)

const Control = ({stack, tab, drawer}) => (
    <View>
        {stack && <ContainerStack/>}
    </View>
)

Control.defaultProps = {
    stack: true,
    tab: false,
    drawer: false
}

export default Control