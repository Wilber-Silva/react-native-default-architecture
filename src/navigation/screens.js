import {
  createStackNavigator, 
  createBottomTabNavigator, 
  createDrawerNavigator,
  createMaterialTopTabNavigator,
  createSwitchNavigator
} from 'react-navigation';

import {Home} from "./../screens";

const Screens = {
  Home: Home,
  Profile: Home,
}

export const stackNavigator = createStackNavigator(
  { ...Screens },
  {
    initialRouteName: "Home"
  }
)

export const tabNavigator = createMaterialTopTabNavigator(
  { ...Screens },
  {
    initialRouteName: "Home"
  }
)

export const drawerNavigator = createDrawerNavigator(
  { ...Screens }
)

export default stackNavigator;