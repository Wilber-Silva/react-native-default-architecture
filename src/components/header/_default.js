import React from "react";
import {View} from "react-native";

import StatusBar from "./../statusBar";
import appBarStyles from "./../../styles/header/appBarStyle";
import {Appbar as AppBar} from "react-native-paper";


const Head = () => (
    <View style={appBarStyles.container}>
        <StatusBar/>

        <AppBar style={appBarStyles.top}>
            <AppBar.Action icon="archive" onPress={() => console.log('Pressed archive')} />
            <AppBar.Action icon="mail" onPress={() => console.log('Pressed mail')} />
            <AppBar.Action icon="label" onPress={() => console.log('Pressed label')} />
            <AppBar.Action icon="delete" onPress={() => console.log('Pressed delete')} />
        </AppBar>
    </View>
)

export default Head;