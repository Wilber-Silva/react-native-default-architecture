import React from "react";
import {StatusBar, View} from "react-native";

const Bar = ({backgroundColor, barStyle, hidden}) => (
    <View>
        <StatusBar 
            backgroundColor={backgroundColor} 
            barStyle={barStyle} 
            translucent={false} 
            animated     
        />
        <View>
            <StatusBar hidden={hidden} />
        </View>
    </View>
)

Bar.defaultProps = {
    backgroundColor: 'blue',
    barStyle: 'light-content',
    hidden: false
}

export default Bar;